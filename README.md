# Data for "Parameter, Noise, and Tree Topology Effects in Tumor Phylogeny Inference"
  
Run `python3 make_plots.py` in the `simulated/` directory to generate all plots. The data for all plots in the paper is contained in pickle files in that directory.


CLL data from:

Schuh, A., Becq, J., Humphray, S., Alexa, A., Burns, A., Clifford, R., Feller, S.M., Grocock, R., Henderson, S., Khrebtukova, I., Kingsbury, Z., Luo, S., McBride, D., Murray, L., Menju, T., Timbs, A., Ross, M., Taylor, J., Bentley, D.: Monitoring chronic lymphocytic leukemia progression by whole genome sequencing reveals heterogeneous clonal evolution patterns. Blood 120(20), 4191–4196 (2012).

ccRCC data from:

Gerlinger, M., Horswell, S., Larkin, J., Rowan, A.J., Salm, M.P.,Varela, I., Fisher, R., McGranahan, N., Matthews, N., Santos, C.R., Martinez, P., Phillimore, B., Begum, S., Rabinowitz, A., Spencer-Dene, B., Gulati, S., Bates, P.A., Stamp, G., Pickering, L., Gore, M., Nicol, D.L., Hazell, S., Futreal, P.A., Stewart, A., Swanton, C.: Genomic architecture and evolution of clear cell renal cell carcinomas defined by multiregion sequencing. Nature Genetics 46(3), 225-233 (2014).


