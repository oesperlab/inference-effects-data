'''
Unless otherwise specified, the parameter settings default to:
10 mutations
5 samples
60x coverage
0 overdispersion
0 epsilon (sum condition relaxation)
'''

import pickle

import matplotlib.pyplot as plt
import numpy as np
import os
import random
from collections import Counter


def plot_param_data():
    print('Loading approximate ancestry graph data...')
    with open('approx_param_data.pickle', 'rb') as handle:
        approx_data = pickle.load(handle)

    print('Loading relaxed sum condition data...')
    with open('epsilon_param_data.pickle', 'rb') as handle:
        epsilon_data = pickle.load(handle)

    print('Loading strict ancestry graph data...')
    with open('strict_param_data.pickle', 'rb') as handle:
        strict_data = pickle.load(handle)

    print('Loading PTR data...')
    with open('ptr_param_data.pickle', 'rb') as handle:
        trans_data = pickle.load(handle)

    rho_values = sorted(strict_data['overdispersion'].keys())
    coverage_values = sorted(strict_data['coverage'].keys())
    k_transitive_values = sorted(trans_data['k_transitive'].keys())
    n_values = sorted(strict_data['n'].keys())
    s_values = sorted(strict_data['s'].keys())
    epsilon_values = sorted(epsilon_data['epsilon'].keys())

    f, axarr = plt.subplots(1, 4, sharex=False, sharey=True, figsize=(12, 2.5))
    i = -1
    for variable, values, display, noise in [
        ('n', n_values, 'Number of Mutations', 0.1),
        ('s', s_values, 'Number of Samples', 0.1),
        ('coverage', coverage_values, 'Coverage', 1),
        ('overdispersion', rho_values, 'Overdispersion Parameter', 0.01)]:
        i += 1
        if variable == 'n':
            values.remove(2)

        output = ''
        for value in values:
            output += '\n'.join([
                '{} = {}'.format(variable, value),
                'solvable: {} / 10000'.format(
                    sum(trial['valid_trees'] > 0 for trial in
                        strict_data[variable][value])
                ),
                'mean A-D improvement: {}'.format(
                     np.nanmean([(trial['mean_rand_ancestor_dist'] -
                                               trial['mean_ancestor_dist']) / trial[
                                                  'mean_rand_ancestor_dist'] for
                                              trial in strict_data[variable][value] if (
                                                          trial[
                                                              'mean_ancestor_dist'] is not None and
                                                          trial[
                                                              'mean_rand_ancestor_dist'] != 0)])
                ),
                'mean valid trees: {}'.format(
                    np.mean([trial['valid_trees'] for trial in strict_data[variable][value]])
                ),
                'max valid trees: {}'.format(
                    np.max([trial['valid_trees'] for trial in strict_data[variable][value]])
                ),
                'mean spanning trees: {}'.format(
                    np.mean([trial['all_trees'] for trial in strict_data[variable][value]])
                ),
                'max spanning trees: {}'.format(
                    np.max([trial['all_trees'] for trial in strict_data[variable][value]])
                )
            ])
            output += '\n\n'

        with open('summary/strict_{}_data_summary.txt'.format(variable), 'w') as f:
            f.write(output)
        print('Saved strict {} data summary to summary/'.format(variable))


        ### STRICT PLOTS ###
        step = values[1] - values[0]

        plt.figure(figsize=(2.5, 2))
        plt.plot(values, [sum(trial['valid_trees'] > 0  for trial in strict_data[variable][value]) / len(strict_data[variable][value]) for value in values], 'bo-')

        plt.ylim(bottom=0, top=1)
        plt.xlabel(display)
        plt.xlim(left=min(values) - step, right=max(values) + step)


        if variable == 'n':
            plt.ylabel('Fraction of Trials')

        if variable == 'overdispersion':
            plt.xticks([0, 0.02, 0.04, 0.06, 0.08], [0, 0.02, 0.04, 0.06, 0.08])
        elif variable == 's':
            plt.xticks([0, 2, 4, 6, 8, 10, 12, 14, 16],
                       [0, 2, 4, 6, 8, 10, 12, 14, 16])
        elif variable == 'n':
            plt.xticks([2, 4, 6, 8, 10, 12], [2, 4, 6, 8, 10, 12])

        elif variable == 'coverage':
            plt.xticks([60, 100, 140, 180], [60, 100, 140, 180])

        plt.savefig('plots/strict_' + variable + '_frac_solvable.pdf', bbox_inches='tight')
        plt.close()
        print('Saved strict {} plots to plots/'.format(variable))


        rand_label = 'mean_rand_ancestor_dist'
        normal_label = 'mean_ancestor_dist'

        for value in values:
            to_box = [(trial[rand_label] - trial[normal_label]) / trial[rand_label] if trial[rand_label] > 0 else  - trial[normal_label] for trial in strict_data[variable][value] if trial[normal_label] is not None ]
            if len(to_box) > 0:
                axarr[i].boxplot(to_box, positions=[value], widths=[step * 0.8],showfliers=False)

        axarr[i].set_xlabel(display)
        axarr[i].set_ylim(bottom=0, top=1.05)

        axarr[0].set_ylabel('A-D Distance Improvement')

        axarr[i].set_xlim(left=min(values)-step, right=max(values)+step)
        if variable == 'overdispersion':
            axarr[i].set_xticks([0, 0.02, 0.04, 0.06, 0.08])
            axarr[i].set_xticklabels([0, 0.02, 0.04, 0.06, 0.08])
            plt.subplots_adjust(wspace=0)
            plt.savefig('plots/strict_' + variable + '_soln_quality.pdf', bbox_inches='tight')
            plt.close()
        elif variable == 's':
            axarr[i].set_xticks([2, 4, 6, 8, 10, 12, 14])
            axarr[i].set_xticklabels([2, 4, 6, 8, 10, 12, 14])
        elif variable == 'n':
            axarr[i].set_xticks([4, 6, 8, 10, 12])
            axarr[i].set_xticklabels([4, 6, 8, 10, 12])

        elif variable == 'coverage':
            axarr[i].set_xticks([60, 100, 140, 180])
            axarr[i].set_xticklabels([60, 100, 140, 180])

        ### COMPARISON PLOTS ###
        prob_rand_label = 'rand_ancestor_dist'
        prob_normal_label = 'ancestor_dist'

        plt.figure(figsize=(2.5, 2))

        plt.plot(values, [np.mean(
            [(trial[rand_label] - trial[normal_label]) / trial[rand_label]
             for trial in strict_data[variable][value] if trial[rand_label]]) for value in values], 'bo-', label='strict')

        plt.plot(values, [np.mean(
            [(trial[prob_rand_label] - trial[prob_normal_label]) / trial[prob_rand_label]
             for trial in approx_data[variable][value] if trial[prob_rand_label]]) for value in values], 'ro-', label='approximate')

        plt.xlabel(display)
        plt.ylim(bottom=0, top=1.05)

        if variable == 'n':
            plt.ylabel('Mean A-D Improvement')

        plt.xlim(left=min(values) - step, right=max(values) + step)
        if variable == 'overdispersion':
            plt.xticks([0, 0.02, 0.04, 0.06, 0.08], [0, 0.02, 0.04, 0.06, 0.08])
        elif variable == 's':
            plt.xticks([0, 2, 4, 6, 8, 10, 12, 14, 16],
                       [0, 2, 4, 6, 8, 10, 12, 14, 16])
        elif variable == 'n':
            plt.xticks([2, 4, 6, 8, 10, 12], [2, 4, 6, 8, 10, 12])

        elif variable == 'coverage':
            plt.xticks([60, 100, 140, 180], [60, 100, 140, 180])
        plt.legend(loc='lower right', fontsize=10)
        plt.savefig('plots/compare_' + variable + '_soln_quality.pdf', bbox_inches='tight')
        plt.close()
        print('Saved strict/approx {} comparison plots to plots/'.format(variable))


    ### PTR DATA ###
    output = ''
    for k in k_transitive_values:
        output += '\n'.join([
            'k = {}'.format(k),
            'sovable: {} / 10000'.format(
                sum(trial['valid_trees'] > 0 for trial in
                       trans_data['k_transitive'][k])
            ),
            'mean A-D improvement: {}'.format(
                 np.nanmean([(trial['mean_rand_ancestor_dist'] -
                                           trial['mean_ancestor_dist']) / trial[
                                              'mean_rand_ancestor_dist'] for
                                          trial in trans_data['k_transitive'][k] if (
                                                      trial[
                                                          'mean_ancestor_dist'] is not None and
                                                      trial[
                                                          'mean_rand_ancestor_dist'] != 0)])
            ),
            'mean valid trees: {}'.format(
                np.mean([trial['valid_trees'] for trial in trans_data['k_transitive'][k]])
            ),
            'max valid trees: {}'.format(
                np.max([trial['valid_trees'] for trial in trans_data['k_transitive'][k]])
            ),
            'mean spanning trees: {}'.format(
                np.mean([trial['all_trees'] for trial in trans_data['k_transitive'][k]])
            ),
            'max spanning trees: {}'.format(
                np.max([trial['all_trees'] for trial in trans_data['k_transitive'][k]])
            )
        ])
        output += '\n\n'

    with open('summary/PTR_data_summary.txt', 'w') as f:
        f.write(output)
    print('Saved PTR data to summary/')


    f, axarr = plt.subplots(1, 2, figsize=(6, 2))

    axarr[0].set_yscale('log')

    axarr[0].plot(range(2, 7), [np.mean([trial['all_trees'] for trial in trans_data['k_transitive'][k]]) for k in range(2, 7)], 'bo-')

    axarr[0].plot([7],
             [np.mean([trial['all_trees'] for trial in trans_data['k_transitive'][k]])
              for k in [10]], 'bo-', label='Mean')

    axarr[0].plot(range(2, 7),
             [np.max([trial['all_trees'] for trial in trans_data['k_transitive'][k]])
              for k in range(2, 7)], 'ro-', label='Max')

    axarr[0].plot([7],
             [np.max([trial['all_trees'] for trial in trans_data['k_transitive'][k]])
              for k in [10]], 'ro-')

    axarr[0].set_xticks([2, 3, 4, 5, 6, 7])
    axarr[0].set_xticklabels([2, 3, 4, 5, 6, 'None'])
    axarr[0].set_xlabel('Transitive Removal Threshold')
    axarr[0].set_ylabel('AG Spanning Trees')
    axarr[0].set_xlim(left=2, right=8)
    axarr[0].legend(loc='lower right', fontsize=10)

    variable = 'k_transitive'
    for value in [2, 3, 4, 5, 6, 10]:
        to_box = [
            (trial[rand_label] - trial[normal_label]) / trial[rand_label] if
            trial[rand_label] > 0 else - trial[normal_label] for trial in
            trans_data[variable][value] if trial[normal_label] is not None]
        if len(to_box) > 0:
            axarr[1].boxplot(to_box, positions=[value if value != 10 else 7], widths=[0.8],showfliers=False)

    axarr[1].set_xlabel('Transitive Removal Threshold')
    axarr[1].set_ylabel('Mean A-D Improvement')

    axarr[1].set_ylim(bottom=0, top=1.05)
    axarr[1].set_xlim(left=1, right=8)

    axarr[1].set_xticks([2, 3, 4, 5, 6, 7])
    axarr[1].set_xticklabels([2, 3, 4, 5, 6, 'None'])

    plt.subplots_adjust(wspace=0.35)
    plt.savefig('plots/ptr_trees_quality.pdf', bbox_inches='tight')
    plt.close()
    print('Saved PTR plots to plots/')


    ### APPROX DATA ###
    for variable, values, display, noise in [
            ('epsilon', epsilon_values, 'Sum Constraint Relaxation', 0.001),
            ('overdispersion', rho_values, 'Overdispersion Parameter', 0.001),
            ('coverage', coverage_values, 'Coverage', 1),
            ('n', n_values, 'Number of Mutations', 0.1),
            ('s', s_values, 'Number of Samples', 0.1)]:

        if variable == 'epsilon':
            data = epsilon_data
        else:
            data = approx_data

        output = ''
        for value in values:
            output += '\n'.join([
                '{} = {}'.format(variable, value),
                'sovable: {} / 10000'.format(
                    sum(trial['valid_trees'] > 0 for trial in
                           data[variable][value])
                ),
                'mean A-D improvement: {}'.format(
                     np.nanmean([(trial['rand_ancestor_dist'] -
                                               trial['ancestor_dist']) / trial[
                                                  'rand_ancestor_dist'] for
                                              trial in data[variable][value] if (
                                                          trial[
                                                              'ancestor_dist'] is not None and
                                                          trial[
                                                              'rand_ancestor_dist'] != 0)])
                ),
                'mean valid trees: {}'.format(
                    np.mean([trial['valid_trees'] for trial in data[variable][value]])
                ),
                'max valid trees: {}'.format(
                    np.max([trial['valid_trees'] for trial in data[variable][value]])
                ),
                'mean spanning trees: {}'.format(
                    np.mean([trial['all_trees'] for trial in data[variable][value]])
                ),
                'max spanning trees: {}'.format(
                    np.max([trial['all_trees'] for trial in data[variable][value]])
                )
            ])
            output += '\n\n'

        with open('summary/approx_{}_data_summary.txt'.format(variable), 'w') as f:
            f.write(output)
        print('Saved approx {} data summary to summary/'.format(variable))

        step = values[1] - values[0]

        plt.figure(figsize=(2.5, 2))
        plt.plot(values, [sum(trial['valid_trees'] > 0 for trial in data[variable][value]) / len(data[variable][value]) for value in values], 'bo-')

        plt.ylim(bottom=0, top=1)
        plt.xlabel(display)
        plt.ylabel('Fraction of Solvable Trials')
        if variable == 'overdispersion':
            plt.xticks([0, 0.02, 0.04, 0.06, 0.08], [0, 0.02, 0.04, 0.06, 0.08])
        elif variable == 's':
            plt.xticks([2, 4, 6, 8, 10, 12, 14], [2, 4, 6, 8, 10, 12, 14])
        elif variable == 'n':
            plt.xticks([2, 4, 6, 8, 10, 12], [2, 4, 6, 8, 10, 12])
        elif variable == 'coverage':
            plt.xticks([60, 100, 140, 180], [60, 100, 140, 180])
        plt.savefig('plots/approx_' + variable + '_frac_solvable.pdf', bbox_inches='tight')
        plt.close()

        rand_label = 'rand_ancestor_dist'
        normal_label = 'ancestor_dist'

        plt.figure(figsize=(2.5, 2))

        for value in values:
            if [trial[rand_label] - trial[normal_label] for trial in data[variable][value] if trial[normal_label] is not None] == []:
                continue
            plt.boxplot([(trial[rand_label] - trial[normal_label])/trial[rand_label] for trial in data[variable][value] if trial[normal_label] is not None and trial[rand_label] != 0], positions=[value], widths=[step * 0.8],showfliers=False)

        plt.xlabel(display)
        plt.ylabel('A-D Improvement')
        plt.xlim(left=min(values)-step, right=max(values)+step)
        if variable == 'overdispersion':
            plt.xticks([0, 0.02, 0.04, 0.06, 0.08], [0, 0.02, 0.04, 0.06, 0.08])
        elif variable == 's':
            plt.xticks([2, 4, 6, 8, 10, 12, 14], [2, 4, 6, 8, 10, 12, 14])
        elif variable == 'n':
            plt.xticks([2, 4, 6, 8, 10, 12], [2, 4, 6, 8, 10, 12])
        elif variable == 'coverage':
            plt.xticks([60, 100, 140, 180], [60, 100, 140, 180])
        
        plt.savefig('plots/approx_' + variable + '_soln_quality.pdf', bbox_inches='tight')
        plt.close()     
        print('Saved approx {} plors to plots/'.format(variable))


    # ### EPSILON DATA ###
    step = epsilon_values[1] - epsilon_values[0]
    output = ''
    for epsilon in epsilon_values:
        output += '\n'.join([
            'epsilon = {}'.format(epsilon),
            'sovable: {} / 10000'.format(
                sum(trial['valid_trees'] > 0 for trial in
                       epsilon_data['epsilon'][epsilon])
            ),
            'mean A-D improvement: {}'.format(
                 np.nanmean([(trial['rand_ancestor_dist'] -
                                           trial['ancestor_dist']) / trial[
                                              'rand_ancestor_dist'] for
                                          trial in epsilon_data['epsilon'][epsilon] if (
                                                      trial[
                                                          'ancestor_dist'] is not None and
                                                      trial[
                                                          'rand_ancestor_dist'] != 0)])
            ),
            'mean valid trees: {}'.format(
                np.mean([trial['valid_trees'] for trial in epsilon_data['epsilon'][epsilon]])
            ),
            'max valid trees: {}'.format(
                np.max([trial['valid_trees'] for trial in epsilon_data['epsilon'][epsilon]])
            ),
            'mean spanning trees: {}'.format(
                np.mean([trial['all_trees'] for trial in epsilon_data['epsilon'][epsilon]])
            ),
            'max spanning trees: {}'.format(
                np.max([trial['all_trees'] for trial in epsilon_data['epsilon'][epsilon]])
            )
        ])
        output += '\n\n'

    with open('summary/epsilon_data_summary.txt', 'w') as f:
        f.write(output)
    print('Saved epsilon data to summary/')

   
    plt.figure(figsize=(2.5, 2))
    plt.plot(epsilon_values, [sum(trial['valid_trees'] > 0  for trial in epsilon_data['epsilon'][epsilon]) / len(epsilon_data['epsilon'][epsilon]) for epsilon in epsilon_values], 'bo-')

    plt.ylim(bottom=0, top=1)
    plt.xlabel(display)
    plt.ylabel('Fraction of Solvable Trials')
    plt.subplots_adjust(bottom=0.15, left=0.2)
    plt.xticks(epsilon_values, epsilon_values)
    plt.savefig('plots/epsilon_frac_solvable.pdf', bbox_inches='tight')
    plt.close()

    rand_label = 'rand_ancestor_dist'
    normal_label = 'ancestor_dist'

    plt.figure(figsize=(2.5, 2))

    for epsilon in epsilon_values:
        if [trial[rand_label] - trial[normal_label] for trial in epsilon_data['epsilon'][epsilon] if trial[normal_label] is not None] == []:
            continue
        plt.boxplot([(trial[rand_label] - trial[normal_label])/trial[rand_label] for trial in epsilon_data['epsilon'][epsilon] if trial[normal_label] is not None and trial[rand_label] != 0], positions=[epsilon], widths=[step * 0.8],showfliers=False)

    plt.subplots_adjust(bottom=0.15, left=0.2)
    plt.xlabel(display)
    plt.ylabel('A-D Improvement')
    plt.xlim(left=min(epsilon_values)-step, right=max(epsilon_values)+step)
    plt.xticks(epsilon_values, epsilon_values)

    plt.savefig('plots/epsilon_soln_quality.pdf', bbox_inches='tight')
    plt.close()     
    print('Saved epsilon plots to plots/'.format(variable))


def plot_strict_topo_data():
    with open('strict_topo_data.pickle', 'rb') as handle:
        data = pickle.load(handle)['n'][10]

    ag_edges = []
    evo_heights = []
    evo_leaves = []
    evo_single_child_fracs = []
    evo_mean_subtree_heights = []
    ad_improvements = []

    ag_edges_existence = {}
    evo_heights_existence = {}
    evo_leaves_existence = {}
    evo_single_child_fracs_existence = {}
    evo_mean_subtree_heights_existence = {}

    ag_edges_count = {}
    evo_heights_count = {}
    evo_leaves_count = {}
    evo_single_child_fracs_count = {}
    evo_mean_subtree_heights_count = {}

    ge_100_evo_leaves = []
    ge_100_evo_heights = []

    count = 0
    for trial in data:
        count += 1
        if trial['evo_height'] not in evo_heights_existence:
            evo_heights_existence[trial['evo_height']] = 0
        if trial['evo_leaves'] not in evo_leaves_existence:
            evo_leaves_existence[trial['evo_leaves']] = 0
        if trial['evo_single_child_frac'] not in evo_single_child_fracs_existence:
            evo_single_child_fracs_existence[trial['evo_single_child_frac']] = 0
        if trial['evo_mean_subtree_height'] not in evo_mean_subtree_heights_existence:
            evo_mean_subtree_heights_existence[trial['evo_mean_subtree_height']] = 0
        if trial['ag_edges'] not in ag_edges_existence:
            ag_edges_existence[trial['ag_edges']] = 0
            
        if trial['evo_height'] not in evo_heights_count:
            evo_heights_count[trial['evo_height']] = 0
        if trial['evo_leaves'] not in evo_leaves_count:
            evo_leaves_count[trial['evo_leaves']] = 0
        if trial['evo_single_child_frac'] not in evo_single_child_fracs_count:
            evo_single_child_fracs_count[trial['evo_single_child_frac']] = 0
        if trial['evo_mean_subtree_height'] not in evo_mean_subtree_heights_count:
            evo_mean_subtree_heights_count[trial['evo_mean_subtree_height']] = 0
        if trial['ag_edges'] not in ag_edges_count:
            ag_edges_count[trial['ag_edges']] = 0

        ag_edges_count[trial['ag_edges']] += 1
        evo_heights_count[trial['evo_height']] += 1
        evo_leaves_count[trial['evo_leaves']] += 1
        evo_single_child_fracs_count[trial['evo_single_child_frac']] += 1
        evo_mean_subtree_heights_count[
            trial['evo_mean_subtree_height']] += 1

        if trial['valid_trees'] == 0:
            continue
        elif trial['valid_trees'] >= 100:
            ge_100_evo_heights.append(trial['evo_height'])
            ge_100_evo_leaves.append(trial['evo_leaves'])

        ag_edges_existence[trial['ag_edges']] += 1
        evo_heights_existence[trial['evo_height']] += 1
        evo_leaves_existence[trial['evo_leaves']] += 1
        evo_single_child_fracs_existence[trial['evo_single_child_frac']] += 1
        evo_mean_subtree_heights_existence[trial['evo_mean_subtree_height']] += 1

        ag_edges.append(trial['ag_edges'])
        evo_heights.append(trial['evo_height'])
        evo_leaves.append(trial['evo_leaves'])
        evo_single_child_fracs.append(trial['evo_single_child_frac'])
        evo_mean_subtree_heights.append(trial['evo_mean_subtree_height'])
        ad_improvements.append((trial['mean_rand_ancestor_dist'] - trial['mean_ancestor_dist']) / trial['mean_rand_ancestor_dist'])

    c = Counter(evo_leaves)
    print('Mean leaves (at least 100 spanning trees):', np.mean(ge_100_evo_leaves))
    print('Mean height (at least 100 spanning trees):', np.mean(ge_100_evo_heights))

    plt.bar(c.keys(), c.values())
    plt.xlabel('Underlying Tree Leaves')
    plt.ylabel('Count')
    plt.savefig('plots/leaf_count_distribution.pdf', bbox_inches='tight')
    plt.close()

    c = Counter(evo_heights)

    plt.bar(c.keys(), c.values())
    plt.xlabel('Underlying Tree Height')
    plt.ylabel('Count')
    plt.savefig('plots/height_distribution.pdf', bbox_inches='tight')

    plt.close()


    # print(count)
    plt.scatter(ag_edges, ad_improvements)
    plt.xlabel('AG Edges')
    plt.ylabel('Mean A-D Improvement')
    for edges in ag_edges_existence.keys():
        plt.scatter(edges,
                    np.mean([ad_improvements[i] for i in range(len(ad_improvements)) if ag_edges[i] == edges]),
                    c='red')

    plt.savefig('plots/ag_edges_quality.pdf', bbox_inches='tight')
    # plt.show()
    plt.close()

    plt.figure(figsize=(2.5, 2))
    values = sorted(set(evo_mean_subtree_heights))
    plt.boxplot(
        [[ad_improvements[i] for i in range(len(ad_improvements)) if evo_mean_subtree_heights[i] == value] for value in values],
        positions=values, showfliers=False, widths=0.075)
    plt.xticks(np.arange(0, 3.01, 0.5), np.arange(0, 3.01, 0.5))
    plt.xlim(left=0, right=2.5)

    plt.xlabel('Evolutionary Tree Mean Subtree Height')
    plt.ylabel('Mean A-D Improvement')

    plt.savefig('plots/evo_mean_subtree_height_quality.pdf', bbox_inches='tight')

    # plt.show()
    plt.close()

    plt.figure(figsize=(2.5, 2))
    values = sorted(set(evo_single_child_fracs))
    plt.boxplot(
        [[ad_improvements[i] for i in range(len(ad_improvements)) if evo_single_child_fracs[i] == value] for value in
         values],
        positions=values, showfliers=False, widths=0.075)
    plt.xticks(np.arange(0, 0.7, 0.2), np.arange(0, 0.7, 0.2).round(1))
    plt.xlim(left=-0.1, right=0.7)

    plt.xlabel('Evolutionary Tree Single Child Fraction')
    plt.ylabel('Mean A-D Improvement')

    plt.savefig('plots/evo_single_child_fracs_quality.pdf', bbox_inches='tight')

    # plt.show()
    plt.close()

    plt.figure(figsize=(2.5, 2))
    eh = sorted(ag_edges_existence.keys())
    plt.plot(eh, [ag_edges_existence[key] / ag_edges_count[key] for key in eh], 'bo-')
    plt.xlabel('AG Edges')
    plt.ylabel('Fraction of Solvable Trials')
    plt.ylim(0, 1)
    plt.savefig('plots/ag_edges_existence.pdf', bbox_inches='tight')

    # plt.show()
    plt.close()


    plt.figure(figsize=(2.5, 2))
    eh = sorted(evo_single_child_fracs_existence.keys())
    plt.plot(eh, [evo_single_child_fracs_existence[key] / evo_single_child_fracs_count[key] for key in eh],  'bo-')
    plt.xlabel('Evolutionary Tree Single Child Fraction')

    plt.ylabel('Fraction of Solvable Trials')
    plt.ylim(0, 1)
    plt.savefig('plots/evo_single_child_frac_existence.pdf', bbox_inches='tight')

    # plt.show()
    plt.close()

    plt.figure(figsize=(2.5, 2))
    eh = sorted(evo_mean_subtree_heights_existence.keys())
    plt.plot(eh, [evo_mean_subtree_heights_existence[key] / evo_mean_subtree_heights_count[key] for key in eh], 'bo-')
    plt.xlabel('Evolutionary Tree Mean Subtree Height')
    plt.ylabel('Fraction of Solvable Trials')
    plt.ylim(0, 1)
    plt.savefig('plots/evo_mean_subtree_height_existence.pdf', bbox_inches='tight')

    # plt.show()
    plt.close()


    # BMC paper

    f, axarr = plt.subplots(2, 2, figsize=(6, 5))

    eh = sorted(evo_heights_existence.keys())
    axarr[0, 0].plot(eh, [evo_heights_existence[key] / evo_heights_count[key] for key in eh], 'bo-')
    # plt.xlabel('Evolutionary Tree Height')
    axarr[0, 0].set_ylabel('Fraction of Solvable Trials')
    axarr[0, 0].set_ylim(0, 1)
    axarr[0, 0].set_xticks(range(2, 8))
    axarr[0, 0].set_xticklabels(range(2, 8))
    axarr[0, 0].set_yticks(np.arange(0, 1.01, 0.2).round(1))
    axarr[0, 0].set_yticklabels(np.arange(0, 1.01, 0.2).round(1))

    eh = sorted(evo_leaves_existence.keys())
    axarr[0, 1].plot(eh, [evo_leaves_existence[key] / evo_leaves_count[key] for key in eh], 'bo-')
    axarr[0, 1].set_ylim(0, 1)
    axarr[0, 1].set_xticks(range(2, 9))
    axarr[0, 1].set_xticklabels(range(2, 9))
    axarr[0, 1].set_yticks(np.arange(0, 1.01, 0.2).round(1))
    axarr[0, 1].set_yticklabels(np.arange(0, 1.01, 0.2).round(1))



    values = range(2, 8)
    axarr[1, 0].boxplot(
        [[ad_improvements[i] for i in range(len(ad_improvements)) if evo_heights[i] == value] for value in values],
        positions=values, showfliers=False)
    axarr[1, 0].set_xlabel('Evolutionary Tree Height')
    axarr[1, 0].set_ylabel('Mean A-D Improvement')
    axarr[1, 0].set_ylim(0, 1)
    axarr[1, 0].set_yticks(np.arange(0, 1.01, 0.2).round(1))
    axarr[1, 0].set_yticklabels(np.arange(0, 1.01, 0.2).round(1))


    values = range(2, 9)
    axarr[1, 1].boxplot(
        [[ad_improvements[i] for i in range(len(ad_improvements)) if evo_leaves[i] == value] for value in values],
        positions=values, showfliers=False)
    axarr[1, 1].set_xlabel('Evolutionary Tree Leaves')
    axarr[1, 1].set_ylim(0, 1)
    axarr[1, 1].set_yticks(np.arange(0, 1.01, 0.2).round(1))
    axarr[1, 1].set_yticklabels(np.arange(0, 1.01, 0.2).round(1))

    plt.subplots_adjust(wspace=0.2, hspace=0.2)

    plt.savefig('plots/height_leaves_existence_quality.pdf', bbox_inches='tight')

    plt.close()



def plot_approx_rank_data():
    with open('approx_rank_data.pickle', 'rb') as handle:
        data = pickle.load(handle)


    print(data[0].keys())

    heights = [[] for _ in range(100)]
    leaves = [[] for _ in range(100)]
    sc_fracs = [[] for _ in range(100)]
    mean_subtree_heights = [[] for _ in range(100)]
    ad_dists = [[] for _ in range(100)]

    weights = [[] for _ in range(100)]


    total = 0
    incomplete = 0
    print(data[0])
    for trial in data:
        total += 1
        if len(trial['heights']) != 100:
            incomplete += 1
            continue

        for i, height in enumerate(trial['heights']):
            heights[i].append(height)
        for i, leaf in enumerate(trial['leaves']):
            leaves[i].append(leaf)
        for i, sc_frac in enumerate(trial['sc_fracs']):
            sc_fracs[i].append(sc_frac)
        for i, mean_subtree_height in enumerate(trial['mean_subtree_heights']):
            mean_subtree_heights[i].append(mean_subtree_height)
        for i, ad_dist in enumerate(trial['ad_dists']):
            ad_dists[i].append(ad_dist)
        for i, weight in enumerate(trial['weights']):
            weights[i].append(weight / np.max(trial['weights']))


    print('Skipped', incomplete, 'of', total)
    plt.figure(figsize=(2.5, 2))
    plt.scatter(range(1, 101), [np.mean(mean_subtree_heights[i]) for i in range(100)], color='blue', marker='.')
    plt.xticks(range(0, 110, 20), range(0, 110, 20))
    plt.xlabel('Spanning Tree Rank')
    plt.ylabel('Mean Subtree Height')
    plt.savefig('plots/approx_rank_mean_subtree_height.pdf', bbox_inches='tight')
    plt.close()


    f, axarr = plt.subplots(1, 2, figsize=(6, 2))

    axarr[0].scatter(range(1, 101), [np.mean(leaves[i]) for i in range(100)], color='blue', marker='.')
    axarr[0].plot([0, 100], [5.54, 5.54], dashes=[2, 2], color='black', alpha=0.5)
    axarr[0].set_xticks(range(0, 110, 20))
    axarr[0].set_xticklabels(range(0, 110, 20))

    axarr[0].set_xlabel('Spanning Tree Rank')
    axarr[0].set_ylabel('Leaf Count')


    mean_heights = [np.mean(heights[i]) for i in range(100)]
    axarr[1].scatter(range(1, 101), mean_heights, color='blue', marker='.')
    axarr[1].plot([0, 100], [3.09, 3.09], dashes=[2, 2], color='black', alpha=0.5)
    axarr[1].set_xticks(range(0, 110, 20))
    axarr[1].set_xticklabels(range(0, 110, 20))

    axarr[1].set_xlabel('Spanning Tree Rank')
    axarr[1].set_ylabel('Mean Height')

    plt.subplots_adjust(wspace=0.3)
    plt.savefig('plots/approx_rank_height_leaves.pdf', bbox_inches='tight')
    plt.close()

    print('Rank 1 mean height:', mean_heights[0])
    print('Rank 25 mean height:', mean_heights[24])
    print('Rank 100 mean height:', mean_heights[99])
    print('Rank 25 to 1 percent decrease:', (mean_heights[24] - mean_heights[0]) / mean_heights[24])
    print('Rank 100 to 25 percent decrease:', (mean_heights[99] - mean_heights[24]) / mean_heights[99])


    plt.figure(figsize=(2.5, 2))
    plt.scatter(range(1, 101), [np.mean(sc_fracs[i]) for i in range(100)], color='blue', marker='.')
    plt.xticks(range(0, 110, 20), range(0, 110, 20))
    plt.xlabel('Spanning Tree Rank')
    plt.ylabel('Single Child Fraction')
    plt.savefig('plots/approx_rank_single_child_fraction.pdf', bbox_inches='tight')
    plt.close()

    plt.figure(figsize=(2.5, 2))
    mean_ad_dists = [np.mean(ad_dists[i]) for i in range(100)]
    plt.scatter(range(1, 101), mean_ad_dists, color='blue', marker='.')
    plt.xticks(range(0, 110, 20), range(0, 110, 20))
    plt.xlabel('Spanning Tree Rank')
    plt.ylabel('AD Distance from Truth')
    plt.savefig('plots/approx_rank_quality.pdf', bbox_inches='tight')

    print('rank 1 mean A-D dist:', mean_ad_dists[0])
    print('rank 25 mean A-D dist:', mean_ad_dists[24])
    print('rank 100 mean A-D dist:', mean_ad_dists[99])

    std_dev_ad_dists = [np.std(ad_dists[i], ddof=1) for i in range(100)]
    print('rank 1 stdev A-D dist:', std_dev_ad_dists[0])
    print('rank 25 stdev A-D dist:', std_dev_ad_dists[24])
    print('rank 100 stdev A-D dist:', std_dev_ad_dists[99])


    plt.close()


def plot_ptr_topo_data():
    with open('ptr_topo_data.pickle', 'rb') as handle:
        data = pickle.load(handle)


    f, axarr = plt.subplots(1, 2, figsize=(6, 2.5))

    pre_valid = []
    pre_ad = []
    pre_height = []
    pre_leaves = []
    pre_scf = []
    pre_msh = []

    pre_leaf_error = []
    pre_height_error = []

    evo_leaves = []
    evo_height = []
    evo_scf = []
    evo_msh = []

    for k in [2, 3, 4, 5]:
        post_valid = []
        post_ad = []
        post_height = []
        post_leaves = []
        post_scf = []
        post_msh = []
        post_leaf_error = []
        post_height_error = []

        trials = 0

        for trial in data['k_transitive'][k]:
            if trial['postprune_valid_trees'] > 0:
                trials += 1
                pre_valid.append(trial['preprune_valid_trees'])
                pre_ad.append((trial['mean_rand_ancestor_dist'] - trial['preprune_mean_ancestor_dist']) / trial['mean_rand_ancestor_dist'])
                pre_height.append(trial['preprune_mean_height'])
                pre_leaves.append(trial['preprune_mean_leaves'])
                pre_scf.append(trial['preprune_mean_scf'])
                pre_msh.append(trial['preprune_mean_subtree_height'])

                post_valid.append(trial['postprune_valid_trees'])
                post_ad.append((trial['mean_rand_ancestor_dist'] - trial['postprune_mean_ancestor_dist']) / trial['mean_rand_ancestor_dist'])
                post_height.append(trial['postprune_mean_height'])
                post_leaves.append(trial['postprune_mean_leaves'])
                post_scf.append(trial['postprune_mean_scf'])
                post_msh.append(trial['postprune_mean_subtree_height'])

                evo_leaves.append(trial['evo_leaves'])
                evo_height.append(trial['evo_height'])
                evo_scf.append(trial['evo_single_child_frac'])
                evo_msh.append(trial['evo_mean_subtree_height'])

        print(k, 'ptr:', trials, 'trials')

        axarr[0].boxplot(post_leaves, showfliers=False, positions=[k], widths=[0.8])
        axarr[1].boxplot(post_height, showfliers=False, positions=[k], widths=[0.8])


        print(k, 'leaves:', np.mean(post_leaves))
        print(k, 'height:', np.mean(post_height))

    print('leaves:', np.mean(pre_leaves))
    print('height:', np.mean(pre_height))
    axarr[0].boxplot(pre_leaves, showfliers=False, positions=[6], widths=[0.8])
    axarr[0].plot([1, 7], [np.mean(evo_leaves)] * 2, dashes=[2, 2], color='black', alpha=0.5)

    axarr[0].set_xticks([2, 3, 4, 5, 6])
    axarr[0].set_xticklabels([2, 3, 4, 5, 'None'])
    axarr[0].set_xlim(1, 7)
    axarr[0].set_xlabel('Transitive Removal Threshold')
    axarr[0].set_ylabel('Mean Leaf Count')

    axarr[1].boxplot(pre_height, showfliers=False, positions=[6], widths=[0.8])
    axarr[1].plot([1, 7], [np.mean(evo_height)] * 2, dashes=[2, 2], color='black', alpha=0.5)
    axarr[1].set_xticks([2, 3, 4, 5, 6])
    axarr[1].set_xticklabels([2, 3, 4, 5, 'None'])
    axarr[1].set_xlim(1, 7)
    axarr[1].set_xlabel('Transitive Removal Threshold')
    axarr[1].set_ylabel('Mean Height')

    plt.subplots_adjust(wspace=0.3)
    plt.savefig('plots/ptr_tree_topo.pdf', bbox_inches='tight')
    plt.close()


if __name__ == '__main__':
    os.makedirs('plots', exist_ok=True)
    os.makedirs('summary', exist_ok=True)
    
    plot_param_data()
    plot_strict_topo_data()
    plot_approx_rank_data()
    plot_ptr_topo_data()
